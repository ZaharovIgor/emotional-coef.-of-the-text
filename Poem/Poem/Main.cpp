#include "Sentiments.h"

void get_word_range_inVecCord(List<Coord> &vecCord, string &str_word, int &left_idx, int &rigth_idx)
{ // binary search in letter map of the sentiments sheet
	left_idx = 0, rigth_idx = vecCord.size() - 1;

	int averege_idx = 0;
	while (left_idx < rigth_idx)
	{
		averege_idx = left_idx + (rigth_idx - left_idx) / 2;

		if (str_word.word[0] <= vecCord[averege_idx].letter)
		{
			rigth_idx = averege_idx;
		}
		else{
			left_idx = averege_idx + 1;
		}
	}

	if (str_word.word[0] == vecCord[rigth_idx].letter){
		//std::cout << vecCord[rigth_idx].letter << " : " << vecCord[rigth_idx].left << " - " << vecCord[rigth_idx].rigth << "\n";

		left_idx = vecCord[rigth_idx].left;
		rigth_idx = vecCord[rigth_idx].rigth;
	}
}


double wordsEmotionalNumb(List<Sent> &sentList, List<string> &poemList, List<Coord> &vecCord)
{ //  binary search in vector with words 
	double emotionalSum = 0;
	int emotionalNumb = 0;

	for (int i = 0; i < poemList.size(); i++)
	{
		if (poemList[i].is_deleted() || poemList[i].is_service()){
			continue;
		}

		int  left_idx = 0, rigth_idx = vecCord.size() - 1;
		get_word_range_inVecCord(vecCord, poemList[i], left_idx, rigth_idx);

		if (left_idx == rigth_idx && poemList[i].compare(sentList[left_idx].word) == 0) {
			std::cout << sentList[rigth_idx].word.word << ' ' << sentList[rigth_idx].rating << "\n";
			emotionalSum += sentList[left_idx].rating;
			continue;
		}

		string comp_word, comp_word2;// for testing! to see comparing words		
		while (left_idx < rigth_idx)
		{
			int averege_idx = left_idx + (rigth_idx - left_idx) / 2;

			comp_word = poemList[i];
			comp_word2 = sentList[averege_idx].word;// for testing

			if (poemList[i].compare(sentList[averege_idx].word) == -1 || poemList[i].compare(sentList[averege_idx].word) == 0) {
				rigth_idx = averege_idx;
			}
			else{
				left_idx = averege_idx + 1;
			}
		}

		if (poemList[i].compare(sentList[rigth_idx].word) == 0) {

			std::cout << sentList[rigth_idx].word.word << ' ' << sentList[rigth_idx].rating << "\n";
			emotionalSum += sentList[rigth_idx].rating;
			//emotionalNumb++;
		}
	}

	return emotionalSum;
}


double phraseEmotionalNumb(List<Sent> &sentList, List<string> &poemList, List<Coord> &vecCord)
{ // binary search in list of phrases
	double emotionalSum = 0;
	int emotionalNumb = 0;

	for (int i = 0; i < poemList.size(); i++)
	{
		if (poemList[i].is_service())
			continue;


		int left_idx = 0, rigth_idx = vecCord.size() - 1;
		get_word_range_inVecCord(vecCord, poemList[i], left_idx, rigth_idx);

		if (left_idx == rigth_idx)
		{
			string comp_phrase = poemList[i];

			if (i + comp_phrase.wordsAmt() < poemList.size()){
				for (int j = 1; j < sentList[left_idx].word.wordsAmt() && i + j < poemList.size(); j++)
				{// adding next words from poem list
					if (poemList[i + j].is_service()){
						comp_phrase += poemList[i + j];
					}
					else{
						comp_phrase += ' ';
						comp_phrase += poemList[i + j];
					}
				}
			}

			if (comp_phrase.compare(sentList[left_idx].word) == 0){
				std::cout << sentList[rigth_idx].word.word << ' ' << sentList[rigth_idx].rating << "\n";
				emotionalSum += sentList[left_idx].rating;
				int j;
				for (j = i; j < i + comp_phrase.wordsAmt(); j++){
					poemList[j].deleteWord(true);
				}
				i = j - 1;
			}
			continue;
		}

		
		string comp_phrase, comp_phrase2;
		while (left_idx < rigth_idx)
		{
			int averege_idx = left_idx + (rigth_idx - left_idx) / 2;

			comp_phrase = poemList[i];	
			comp_phrase2 = sentList[averege_idx].word;// for testing

			for (int j = 1; j < sentList[averege_idx].word.wordsAmt() && i + j < poemList.size(); j++)
			{// adding next words from poem list
				if (poemList[i + j].is_service()){				
					comp_phrase += poemList[i + j];
				}
				else{
					comp_phrase += ' ';
					comp_phrase += poemList[i + j];
				}				
			}
			
			
			bool zero_case = false;
			switch (comp_phrase.compare(sentList[averege_idx].word))
			{
				case -1: rigth_idx = averege_idx; break;
				case 1: left_idx = averege_idx + 1; break;
				case 0: zero_case = true; break;
			}
			
			if (zero_case){
				rigth_idx = averege_idx;
				break;
			}	
		}


		if (comp_phrase.compare(sentList[rigth_idx].word) == 0)
		{
			string overlap_phrase = comp_phrase;
			int overlap_bias = 0;
			for (int bias = 0;  i + comp_phrase.wordsAmt() < poemList.size(); bias++) {
				
				bool no_index_err = rigth_idx + bias + 1 < sentList.size();
				if (no_index_err && !strncmp(sentList[rigth_idx].word.word,
											 sentList[rigth_idx + bias + 1].word.word,
											 strlen(sentList[rigth_idx].word.word)))
				{
					int comp_words_amt = comp_phrase.wordsAmt(),
						sent_words_amt = sentList[rigth_idx + bias + 1].word.wordsAmt();
					string temp = comp_phrase;

					for (int k = 0; k < sent_words_amt - comp_words_amt; k++)
						if (poemList[i + k].is_service()){
							temp += poemList[i + comp_words_amt + k];
						}
						else{
							temp += ' ';
							temp += poemList[i + comp_words_amt + k];
						}

					if (!temp.compare(sentList[rigth_idx + bias + 1].word)) {
						overlap_phrase = temp;
						overlap_bias = bias + 1;
					}
				}
				else
					break;
			}

			if (!overlap_phrase.compare(sentList[rigth_idx + overlap_bias].word)) {

				std::cout << sentList[rigth_idx + overlap_bias].word.word << ' ' << sentList[rigth_idx + overlap_bias].rating << "\n";
				emotionalSum += sentList[rigth_idx + overlap_bias].rating;
				//emotionalNumb++;
			}
			else {
				std::cout << sentList[rigth_idx].word.word << ' ' << sentList[rigth_idx].rating << "\n";
				emotionalSum += sentList[rigth_idx].rating;
				//emotionalNumb++;
			}

			int j;
			for (j = i; j < i + sentList[rigth_idx + overlap_bias].word.wordsAmt() && j < poemList.size(); j++){
				poemList[j].deleteWord(true);
			}
			i = j - 1;
		}
	}

	return emotionalSum;
}


int main(void)
{
	List<Sent> wordsList, phraseList;
	List<string> poemList;
	List<Coord> wordsCordList, phraseCordList;

	try
	{
		char* sentimentsFileName = "test_sentiments.txt";
		readSentFromFile(wordsList, phraseList, sentimentsFileName);

		char* poemFileName = "test_poem.txt";
		readPoemFromFile(poemList, poemFileName);
	}
	catch (char* error)
	{
		std::cout << error;
		system("pause");
		return 0;
	}

	makeFirstLetterMap(wordsCordList, wordsList);
	makeFirstLetterMap(phraseCordList, phraseList);

	std::cout << phraseList.size() << "\n"; 

	std::cout << "Emotional coeficents of the text is\n";
	std::cout << phraseEmotionalNumb(phraseList, poemList, phraseCordList) << "\n";
	std::cout << wordsEmotionalNumb(wordsList, poemList, wordsCordList) << std::endl;
	
	system("pause");
	return 0;
}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         