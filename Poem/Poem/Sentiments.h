#include <iostream>
#include <fstream>
#include "MyString.h"
//#include "MyLIFO.h" 
#include "MyDNS.h"

#ifndef SENT_H
#define SENT_H


struct Sent
{
public:

	string word;
	double rating;

	Sent()
	{

	}

	Sent(string word, double rating)
	{
		this->word = word;
		this->rating = rating;
	}

	void print()
	{
		std::cout << word.word << " " << rating << "\n";
	}
};

struct Coord
{
public:

	char letter;
	int left;
	int rigth;

	Coord()
	{

	}

	Coord(char letter, int left, int rigth)
	{
		this->letter = letter;
		this->left = left;
		this->rigth = rigth;
	}

	void print()
	{
		std::cout << letter << " : " << left << " - " << rigth << "\n";
	}
};

void readSentFromFile(List<Sent> &vecWords, List<Sent> &vecPhrase, char*);
void readPoemFromFile(List<string> &vecPoem, char*);
void makeFirstLetterMap(List<Coord> &vecCord, List<Sent> &vecSent);
#endif