#include <string.h>

#ifndef MY_STRING
#define MY_STRING


class string
{
private:

	int const max_size = 256;
	int s_size;
	bool word_deleted;
	bool service_symb;

public:

	char word[256];

	string()
	{
		memset(word, '\0', max_size);
		s_size = 0;
		word_deleted = false;
		service_symb = false;
	}

	void operator=(string &newStr)
	{
		strcpy_s(word, newStr.word);
		s_size = strlen(newStr.word);
	}

	void operator+=(char symb)
	{
		word[s_size] = symb;
		word[s_size + 1] = '\0';
		s_size = strlen(word);
	}

	void operator+=(string &str)
	{
		strcat_s(word, str.word);
		s_size = strlen(word);
	}

	int compare(string& comp_str)
	{
		return strcmp(word, comp_str.word);
	}

	void toLowerCase()
	{ // reset symbol from upper to lower case
		for (int i = 0; i < s_size; i++)
		{
			if (word[i] >= 'A' && word[i] <= 'Z')
			{
				word[i] += 32;
			}
		}
	}

	int wordsAmt()
	{ // counted words amount by amt of whitespaces
		int leng = 1;

		for (int i = 0; i < strlen(word); i++)
		{
			if (word[i] == ' ')
				leng++;
		}

		return leng;
	}

	void deleteWord(bool flag)
	{ // set object as deteled
		word_deleted = flag;
	}

	bool is_deleted()
	{
		return word_deleted;
	}

	
	bool empty()
	{
		return strlen(word) == 0;
	}


	void set_as_service()
	{ // set object as service symbol
		service_symb = true;
	}

	bool is_service()
	{
		return service_symb;
	}
};

#endif
