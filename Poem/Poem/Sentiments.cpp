#include "Sentiments.h"
#pragma once

bool normalSymb(char &symb, char *badSymb, int size)
{
	for (short i = 0; i < size; i++)
	{
		if (symb == badSymb[i])
			return false;
	}

	return true;
}

void readSentFromFile(List<Sent> &wordsList, List<Sent> &phraseList, char* sentimentsFileName)
{
	std::ifstream inFile(sentimentsFileName, std::ios_base::in);

	if (!inFile.is_open())
	{
		throw "File with sentiments is not found!\n";
	}

	while (!inFile.eof())
	{
		string word;
		char symb;
		for (int i = 0;; i++)
		{
			inFile.get(symb);

			if (symb != ',' && symb != '\n' && symb != '\t')
			{
				word += symb;
			}
			else
				break;
		}

		if (!word.empty())
		{
			double rating;
			inFile >> rating;

			Sent buffSent(word, rating);

			if (word.wordsAmt() == 1)
				wordsList.push_back(buffSent);
			else
				phraseList.push_back(buffSent);
		}
	}
	
	std::cout << "Text File:'" << sentimentsFileName << "' has been read from disk\n";
	inFile.close();
}

void readPoemFromFile(List<string> &poemList, char* poemFileName)
{
	std::ifstream inFile(poemFileName, std::ios_base::in);

	if (!inFile.is_open()){
		throw "File with poem is not found!\n";
	}

	char badSymb[] = { ',', '.', '!', '?', '*', ' ', '(', ')', ';', ':', '\t', '\n' };
	char serviceSymb[] = { '.', '!', '?', ';' };
	char symb = ' ';
	
	while (!inFile.eof())
	{
		string word;	

		if (!normalSymb(symb, serviceSymb, 4)){
			string service;
			service += symb;
			service.set_as_service();
			
			poemList.push_back(service);
		}

		for (int i = 0;; i++)
		{
			inFile.get(symb);

			if (normalSymb(symb, badSymb, 12) && !inFile.eof())
			{
				word += symb;
			}
			else
				break;
		}

		if (!word.empty()){
			word.toLowerCase();
			poemList.push_back(word);
		}
	}

	std::cout << "Text File:'" << poemFileName << "' has been read from disk\n";
	inFile.close();
}

void makeFirstLetterMap(List<Coord> &cordList, List<Sent> &sentList)
{ //searching intervals for every letter or sign
	for (int i = 0; i < sentList.size(); i++)
	{
		char bufChar = sentList[i].word.word[0];

		int last_i = i;
		for (i; i < sentList.size() - 1; i++)
		{
			if (sentList[i].word.word[0] != sentList[i + 1].word.word[0])
			{
				break;
			}
		}

		Coord bufCord(bufChar, last_i, i);
		cordList.push_back(bufCord);
	}
}