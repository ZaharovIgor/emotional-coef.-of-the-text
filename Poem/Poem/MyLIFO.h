#ifndef MY_STACK
#define MY_STACK

/* template class 'vector' - stack LIFO
with basic std::vector functions
include this header after
including <iostream> library
27.03.2017
*/

template <class TYPE>
class vector
{
private:

	vector* node;
	TYPE value;
	int s_size = 0;

public:

	vector(){}

	TYPE& operator[](int i)
	{
		vector* temp = node;

		if (i < 0 || i >= s_size)
			throw "Going out of the range of stack!\n";

		for (int k = s_size - 1; k >= 0; k--)
		{
			if (i == k)
				return temp->value;

			temp = temp->node;
		}
	}

	void push_back(TYPE value); // put value on top of list
	TYPE pop_back(); // pushes out value from top of list
	TYPE back(); // return value from top of list
	bool is_empty(); // true - if list is empty
	void peek(TYPE value); // change value on top of list
	void printStack(); // print list on console in colomn 
	int size(); // return list LIFO size
};

template <class TYPE>
int vector<TYPE>::size()
{
	return s_size;
}

template <class TYPE>
void vector<TYPE>::push_back(TYPE value)
{
	vector* temp = new vector;
	temp->value = value;
	temp->node = node;
	node = temp;
	s_size++;
}

template <class TYPE>
TYPE vector<TYPE>::pop_back()
{
	if (s_size == 0)
		throw "No operands in stack to push out!\n";

	TYPE poped = node->value;
	vector* temp = node;
	node = node->node;
	s_size--;

	delete temp;
	return poped;
}

template <class TYPE>
TYPE vector<TYPE>::back()
{
	return node->value;
}

template <class TYPE>
bool vector<TYPE>::is_empty()
{
	return s_size == 0;
}

template <class TYPE>
void vector<TYPE>::peek(TYPE value)
{
	this->node->value = value;
}

template <class TYPE>
void vector<TYPE>::printStack()
{
	vector* temp = node;

	for (int i = 0; i < s_size; i++)
	{
		std::cout << temp->value << '\n';
		temp = temp->node;
	}
}

#endif