#ifndef MY_DNS
#define MY_DNS

/* my piesant container
with basic functions
like in STL std::List
but without iterators */


template <class TYPE>
struct Field
{ // main fields of List container
	TYPE value;
	Field* previos;
	Field* next;
};


template <class TYPE>
class List
{ // my guerrillian container, DNS List
private:

	int l_size;
	Field<TYPE>* begin;
	Field<TYPE>* end;

public:

	List()
	{
		l_size = 0;
	}

	void push_back(TYPE);
	void push_front(TYPE);
	TYPE pop_back();
	TYPE pop_front();
	TYPE back();
	int size();
	bool empty();

	TYPE& operator[](int i);
};


template <class TYPE>
TYPE& List<TYPE>::operator[](int i)
{
	if ((i < l_size / 2) && i < l_size)
	{
		Field<TYPE>* temp = begin;

		for (int j = 0; j < i; j++){
			temp = temp->next;
		}

		return temp->value;
	}
	else
		if (i < l_size)
		{
			Field<TYPE>* temp = end;

			for (int j = 0; j < l_size - i - 1; j++) {
				temp = temp->previos;
			}

			return temp->value;
		}
}


template <class TYPE>
void List<TYPE>::push_back(TYPE value)
{
	if (l_size == 0)
	{
		Field<TYPE>* temp = new Field<TYPE>;
		temp->value = value;
		temp->next = NULL;
		temp->previos = NULL;

		begin = end = temp;
		l_size++;
	}
	else
	{
		Field<TYPE>* temp = new Field<TYPE>;
		temp->value = value;
		temp->next = NULL;
		temp->previos = end;

		end->next = temp;
		//end->previos = end; mistake in lesson example
		end = temp;
		l_size++;
	}
}


template <class TYPE>
void List<TYPE>::push_front(TYPE value)
{
	if (l_size == 0)
	{
		Field<TYPE>* temp = new Field<TYPE>;
		temp->value = value;
		temp->next = NULL;
		temp->previos = NULL;

		begin = end = temp;
		l_size++;
	}
	else
	{
		Field<TYPE>* temp = new Field<TYPE>;
		temp->value = value;
		temp->next = begin;
		temp->previos = NULL;

		begin->previos = temp;
		begin = temp;
		l_size++;
	}
}


template <class TYPE>
TYPE List<TYPE>::pop_back()
{
	Field<TYPE>* temp = end;

	TYPE return_value = end->value;

	end = end->previos;
	//end->next = NULL;

	delete temp;
	l_size--;

	return return_value;
}


template <class TYPE>
TYPE List<TYPE>::pop_front()
{
	Field<TYPE>* temp = begin;

	TYPE return_value = begin->value;

	begin = begin->next;
	begin->previos = NULL;

	delete temp;
	l_size--;

	return return_value;
}


template <class TYPE>
TYPE List<TYPE>::back()
{
	return end->value;
}


template <class TYPE>
int List<TYPE>::size()
{
	return l_size;
}


template <class TYPE>
bool List<TYPE>::empty()
{
	return l_size == 0;
}

#endif